package com.example.toshiba.vhealth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class Control extends AppCompatActivity {

    private Button kiri,kanan;
    private TextView text;
    String tamp;

    int readBufferPosition;
    byte[] readBuffer;
    Thread workerThread;
    private BluetoothAdapter BA;
    private Set<BluetoothDevice> pairedDevices;
    private BluetoothSocket Socket;
    private InputStream inputstream;
    private OutputStream outputstream;
    volatile boolean stopWorker=true;
    byte blutut=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        BA = BluetoothAdapter.getDefaultAdapter();
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar1);
        kiri = (Button) findViewById(R.id.button);
        kanan = (Button) findViewById(R.id.button2);
        text = (TextView) findViewById(R.id.textView);

        assert seekBar != null;
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                text.setText(Integer.toString(progress));
                if(!stopWorker) {
                    tamp = Integer.toString(progress);
                    ok();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                text.setText(Integer.toString(progress));
                if(!stopWorker) {
                    tamp = Integer.toString(progress);
                    ok();
                }
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                text.setText(Integer.toString(progress));
                if(!stopWorker) {
                    tamp = Integer.toString(progress);
                    ok();
                }
            }
        });

        kiri.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;
            @Override public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, 50);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        sendData("-"); sendData("-"); sendData("-");
                        break;
                }
                return true;
            }

            Runnable mAction = new Runnable() {
                @Override public void run() {
                    sendData("@");
                    mHandler.postDelayed(this, 50);
                }
            };

        });

        kanan.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;
            @Override public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, 50);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        sendData("-"); sendData("-"); sendData("-");
                        break;
                }
                return true;
            }

            Runnable mAction = new Runnable() {
                @Override public void run() {
                    sendData("#"); //sendData("\n");
                    mHandler.postDelayed(this, 50);
                }
            };

        });
    }

    public void bton() {
        if (BA == null) {
            Toast t = Toast.makeText(this, "No bluetooth adapter available", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        }
        if (!BA.isEnabled()) {
            BA.enable();
            Toast t = Toast.makeText(getApplicationContext(), "Tekan Sekali Lagi", Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        }
        if (BA.isEnabled()) {
            if (blutut != 0) {
                Toast t = Toast.makeText(getApplicationContext(), "Sudah Terhubung", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            } else {
                try {
                    listBT();
                    beginListenForData();
                } catch (Exception e) {
                }
            }
        }
    }

    public boolean listBT() {
        pairedDevices = BA.getBondedDevices();
        for (BluetoothDevice bt : pairedDevices) {
            if (bt.getName().toString().equals("QLVSS")) {
                try {
                    Toast t = Toast.makeText(getApplicationContext(), "Terhubung", Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
                    Socket = bt.createRfcommSocketToServiceRecord(uuid);
                    Socket.connect();
                    outputstream = Socket.getOutputStream();
                    inputstream = Socket.getInputStream();
                    blutut = 1;
                    return false;
                } catch (IOException e) {
                    Toast t = Toast.makeText(getApplicationContext(), "Terjadi Kesalahan. Pastikan Alat Dalam Keadaan Menyala" +
                            " dan Restart Program.", Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        }
        return true;
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character
        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {
                        int bytesAvailable = inputstream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            inputstream.read(packetBytes);
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            //pwm.setText(data+" km/jam");
                                        }
                                    });
                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                    } catch (NullPointerException n) {
                        stopWorker = true;
                    }
                }
            }
        });
        workerThread.start();
    }

    private void sendData(String message) {
        byte[] msgBuffer = message.getBytes();

        try {
            outputstream.write(msgBuffer);
        } catch (IOException e) {
        }
    }

    public void ok() {
        tamp += '#';
        sendData(tamp);
        tamp="";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.bton)
//        {
//            bton();
//        }
//        if (id == R.id.btoff)
//        {
//            try {
//                stopWorker = true;
//                if (Socket.isConnected()) {
//                    Socket.close();
//                    blutut=0;
//                }
//            } catch (Exception e){}
//
//            try{bton();}
//            catch (Exception e){Toast.makeText(getApplicationContext(),"Bluetooth OFF",Toast.LENGTH_SHORT);}
//        }
        return super.onOptionsItemSelected(item);
    }
}
