package com.example.toshiba.vhealth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;


import com.eyro.mesosfer.GetCallback;
import com.eyro.mesosfer.MesosferData;
import com.eyro.mesosfer.MesosferException;
import com.eyro.mesosfer.MesosferObject;
import com.eyro.mesosfer.SaveCallback;

import java.util.Locale;

public class DataFormActivity extends AppCompatActivity {
    public static final int INTENT_REQUEST_CODE = 5342;
    public static final int FORM_MODE_ADD = 0;
    public static final String INTENT_FORM_MODE = "IntentFormMode";
    private int formMode;
    private String keasaman, oksigen, salinitas, temperatur;
    private boolean isMamals;
    private ProgressDialog loading;
    private AlertDialog dialog;
    private EditText pH, DO, SAL, Temp;
    public static final int FORM_MODE_EDIT = 1;
    public static final String INTENT_OBJECT_ID = "IntentObjectId";
    private MesosferData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_form);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Data Form");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pH = (EditText) findViewById(R.id.pH);
        DO = (EditText) findViewById(R.id.DO);
        SAL = (EditText) findViewById(R.id.SAL);
        Temp = (EditText) findViewById(R.id.Temp);


        loading = new ProgressDialog(this);
        loading.setIndeterminate(true);
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);

        formMode = getIntent().getIntExtra(INTENT_FORM_MODE, FORM_MODE_ADD);
        if (formMode == FORM_MODE_EDIT) {
            String objectId = getIntent().getStringExtra(INTENT_OBJECT_ID);
            fetchData(objectId);
        }
    }

    private void fetchData(String objectId) {
        loading.setMessage("Fetching data...");
        loading.show();
        MesosferData.createWithObjectId(objectId).fetchAsync(new GetCallback<MesosferData>() {
            @Override
            public void done(MesosferData data, MesosferException e) {
                loading.dismiss();

                if (e != null) {
                    dialog = new AlertDialog.Builder(DataFormActivity.this)
                            .setTitle("Error Happen")
                            .setMessage(
                                    String.format(Locale.getDefault(),
                                            "Error code: %d\nDescription: %s", e.getCode(), e.getMessage()))
                            .show();
                    return;
                }

                DataFormActivity.this.data = data;
                updateView();
            }
        });
    }

    private void updateView() {
        if (data != null) {
            MesosferObject object = data.getData();
            pH.setText(object.optString("pH"));
            DO.setText(object.optString("do"));
            SAL.setText(object.optString("sal"));
            Temp.setText(object.optString("temp"));
        }
    }

    public void handleSave(View view) {
        keasaman = pH.getText().toString();
        oksigen = DO.getText().toString();
        temperatur = Temp.getText().toString();
        salinitas = SAL.getText().toString();

        if (!isInputValid()) {
            return;
        }
        saveData();
    }

    private boolean isInputValid() {
        if (TextUtils.isEmpty(keasaman)) {
            Toast.makeText(this, "pH is empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(oksigen)) {
            Toast.makeText(this, "DO is empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(salinitas)) {
            Toast.makeText(this, "SAL is empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(temperatur)) {
            Toast.makeText(this, "Temperature is empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void saveData() {
        // showing a progress dialog loading
        loading.setMessage("Saving Water Quality Data..");
        loading.show();

        MesosferData data = MesosferData.createData("Log");
        data.setData("pH", Float.valueOf(keasaman));
        data.setData("do", Float.valueOf(oksigen));
        data.setData("sal", Float.valueOf(salinitas));
        data.setData("temp", Float.valueOf(temperatur));

        data.saveAsync(new SaveCallback() {
            @Override
            public void done(MesosferException e) {
                // hide progress dialog loading
                loading.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(DataFormActivity.this);
                builder.setNegativeButton(android.R.string.ok, null);
                // check if there is an exception happen
                if (e != null) {
                    builder.setTitle("Error Happen");
                    builder.setMessage(
                            String.format(Locale.getDefault(), "Error code: %d\nDescription: %s",
                                    e.getCode(), e.getMessage())
                    );
                    dialog = builder.show();
                    return;
                }
                Toast.makeText(DataFormActivity.this, "Data saved", Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }
}
